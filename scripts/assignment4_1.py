import csv

# Creates new spreadsheet called 'episodeDataEdited.csv'.
f = csv.writer(open('episodeDataEdited.csv', 'w'))
# Write header column to new CSV.
f.writerow(['bib', 'creation_date', 'corporate', 'date1', 'director', 'duration', 'language', 'oclc', 'publisher', 'title'])


filename = 'episodeData.csv'
with open(filename) as itemMetadataFile:  # Opens CSV
    itemMetadata = csv.DictReader(itemMetadataFile)  # Reads CSV as dictionary.
    for row in itemMetadata:  # Loops through each row as dictionary.
        # Assigns variables to each value in dictionary.
        bib = row['bib']
        oclc = row['oclc']
        corporate = row['corporate']
        publisher = row['publisher']
        title = row['title']
        duration = row['duration']
        creation_date = row['creation_date']
        date = row['date1']
        language = row['lang']
        director = row['director']
        # Prepends URL to bib number.
        bib = 'https://library.edu/catalog/bib _'+bib
        # Splits corporate variable at "|" into a list.
        corporateList = corporate.split('|')
        # Create blank list.
        newCorporate = []
        # Loops through corporateList.
        for corp in corporateList:
            # Removes period from end of name.
            # Adds name without period to newCorporate list.
            if corp[-1] == '.':
                corp = corp[:-1]
                newCorporate.append(corp)
            else:
                corp = corp
                newCorporate.append(corp)
        # Convert newCorporate list into string with items separated by pipes.
        newCorporate = '|'.join(newCorporate)
        # Splits duration into list at ':'.
        duration = duration.split(':')
        # Create variable for each component of duration.
        # Add hour, minute, second labels.
        duration1 = duration[0]+' hours '
        duration2 = duration[1]+' minutes '
        duration3 = duration[2]+' seconds'
        # Combine variables.
        duration = duration1+duration2+duration3
        # Remove last character from publisher.
        publisher = publisher[:-1]
        # Splits director variable at "|" into a list.
        directorList = director.split('|')
        # Create blank list.
        newDirectorList = []
        # Loops through directorList.
        for director in directorList:
            # Turns director name into list, splitting at each space.
            directorName = director.split(' ')
            print(directorName)
            # Sees how many elements are in directorName.
            # Reorders name based on number of elements.
            # Adds reordered name to newDirectorList.
            if len(directorName) == 2:
                directorFirst = directorName[0].strip()
                directorLast = directorName[1].strip()
                newDirector = directorLast+', '+directorFirst
                newDirectorList.append(newDirector)
            elif len(directorName) == 3:
                directorFirst = directorName[0].strip()
                directorMiddle = directorName[1].strip()
                directorLast = directorName[2].strip()
                newDirector = directorLast+', '+directorFirst+' '+directorMiddle
                newDirectorList.append(newDirector)
            else:
                newDirector = director
                newDirectorList.append(newDirector)
        # Convert newDirector list into string with items separated by pipes.
        newDirector = '|'.join(newDirectorList)
        # Write variables to new spreadsheet. 
        f.writerow([bib, creation_date, newCorporate, date, newDirector, duration, language, oclc, publisher, title])
